#include "polynomial.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>


int main(void)
{
	// utftest();

	polynomial* thePoly = make_term(-6,1);
	// polynomial* otherPoly = make_term(3,2);

	add_to_poly(&thePoly,3,2);
	add_to_poly(&thePoly,5,4);
	// add_to_poly(&thePoly,5,3);
	// add_to_poly(&thePoly,5,4);
	// add_to_poly(&thePoly,5,6);
	
	// printf("Other Poly: ");
	// poly_print(otherPoly);
	// printf("\n");

	// add_to_poly(&thePoly,5,0);
	

	polynomial* tempPoly = make_term(7,0);
	add_to_poly(&tempPoly,-1,1);
	// add_to_poly(&tempPoly,5,2);
	// add_to_poly(&tempPoly,5,3);
	// add_to_poly(&tempPoly,5,4);
	// add_to_poly(&tempPoly,5,6);
	

	
	printf("The Poly: ");
	poly_print(thePoly);
	printf("\n");

	printf("Temp Poly: ");
	poly_print(tempPoly);
	printf("\n");

	polynomial* product = multiply_poly(thePoly,tempPoly);
	printf("The product:");
	poly_print(product);
	printf("|\n");
	
	polynomial* theSum = add_poly(thePoly,tempPoly);
	printf("The Sum: ");
	poly_print(theSum);
	printf("\n");

	polynomial* theDifference = sub_poly(thePoly,tempPoly);
	printf("The Difference: ");
	poly_print(theDifference);
	printf("\n");


	char* aString = poly_to_string(thePoly);
	printf("\nThe Poly as string:%s\n",aString);

	bool equality = is_equal(thePoly,tempPoly);
	printf("Is thePoly and tempPoly equal? %s\n",equality ? "true" : "false");
	// printf("Are they equal? %B",equality);

	printf("\nTransform Poly: ");
	poly_print(thePoly);
	printf("\n");
	apply_to_each_term(thePoly,transform);
	printf("Transform Poly: ");
	poly_print(thePoly);
	printf("\n");

	printf("\nThe Poly: ");
	poly_print(thePoly);
	printf("\n");

	double answer = eval_poly(thePoly,1);
	printf("\nThe answer is:%.1lf\n",answer);


	// wchar_t* testString = toSuper(10);
	// printf("\nTest String: %ls\n",(wchar_t*)testString);

	// printf("\n\nUTF Poly: ");
	// poly_print(thePoly);
	// printf("\n");

	// utftest();


	return(0);
}