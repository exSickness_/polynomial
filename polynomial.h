#ifndef polynomial_h
	#define polynomial_h
	#include <stdbool.h>
	#include <wchar.h>

struct term{
	int coeff;
	int exp;
	struct term *next;
};

typedef struct term polynomial;

struct term *make_term(int coeff, int exp);
void poly_free(polynomial *eqn);
void poly_print(polynomial* eqn);

char* poly_to_string(polynomial* p);
polynomial* add_poly(polynomial* a, polynomial* b);
polynomial* sub_poly(polynomial* a, polynomial* b);
bool is_equal(polynomial* a, polynomial* b);
void apply_to_each_term(polynomial* p, void (*transform)(struct term* ));
double eval_poly(polynomial* a, double x);

polynomial* multiply_poly(polynomial* a, polynomial* b);

void append_poly(polynomial* a, polynomial* b);
void remove_poly(polynomial** list);
void destroy_poly(polynomial* list);
int cmp(int x, int y);
void add_to_poly(polynomial** list, int coeff, int exp);
void transform(polynomial* p);
wchar_t* toSuper(int exponent);
void removeNext(polynomial** list);

#endif