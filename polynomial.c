#include "polynomial.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <locale.h>

/* IMPORTANT: 
 * setlocale(LC_ALL, ""); // MUST BE SET BEFORE UTF-8 EXPONENTS CAN BE PRINTED
 * Set in make_term()
 */

char* poly_to_string(polynomial* p)
{
	polynomial* head = p;
	int tempCo = 0;
	int tempEx = 0;
	
	int sz = 25; // starting size for buffer
	char* buffer = malloc(sz);
	char* eachTerm = malloc(sz);

	while(head)
	{
		tempCo = head->coeff;
		tempEx = head->exp;

		sprintf(eachTerm,"+%dx^%d ",tempCo,tempEx);
		buffer = realloc(buffer,(sz+strlen(eachTerm)));
		strcat(buffer,eachTerm);

		sz += strlen(eachTerm);
		head = head->next;
	}

	return(buffer);
}

bool is_equal(polynomial* a, polynomial* b)
{
	while( a && b)
	{
		if((cmp(a->coeff, b->coeff) != 0) && (cmp(a->exp, b->exp) != 0)) // check for like coef's and exps
		{
			return(false);
		}
		a = a->next;
		b = b->next;
	}

	return(true);
}

void apply_to_each_term(polynomial* p, void (*transform)(struct term* ))
{
	if(p->next)
	{
		apply_to_each_term(p->next, transform ); // recursion!
	}
	(*transform)(p);
}

double eval_poly(polynomial* a, double x)
{
	polynomial* head = a;
	double answer = 0;
	while(head)
	{
		answer += ( (head->coeff) * (pow(x,head->exp)) ); // solves a term
		head = head->next;
	}
	return answer;
}

polynomial* multiply_poly(polynomial* a, polynomial* b)
{
	polynomial* a_head = a;
	polynomial* b_head = b;
	polynomial* product = make_term(0,0);

	while(a)
	{
		while(b)
		{
			int tempCo = a->coeff * b->coeff;
			int tempEx = a->exp + b->exp;
			polynomial* temp = make_term(tempCo, tempEx);
			append_poly(product,temp); // Add the product of the terms to the answer
			b = b->next;
		}
		b = b_head;
		a = a->next;
	}
	a = a_head;
	b = b_head;
	
	remove_poly(&product); // remove the first term
	
	polynomial* p = product;
	while((p->next) && (p) ) // loop to add terms with same exponent
	{
		if (p->exp == p->next->exp)
		{
			p->coeff = p->coeff + p->next->coeff;
			removeNext(&p);
			continue;
		}
		p = p->next;
	}

	return(product);
}

polynomial* add_poly(polynomial* a, polynomial* b)
{
	polynomial* sum = make_term(0,0);
	polynomial* a_head = a;
	polynomial* b_head = b;
	int sumCo = 0;

	while(a)
	{
		if( cmp( a->exp,b->exp)  == 0)
		{
			sumCo  = ((a->coeff) + (b->coeff));
			polynomial* temp = make_term(sumCo, a->exp);
			append_poly(sum,temp);

			a_head = a_head->next;
			a = a_head;

			if(b->next) // check for more b
			{
				b_head = b->next;
				b = b_head;
			}
			else
			{
				while(a) // no more b. adding rest of a to the answer
				{
					polynomial* temp2 = make_term(a->coeff, a->exp);
					append_poly(sum,temp2);
					a = a->next;
				}
			}
		}
		else if( cmp( a->exp,b->exp)  == 1)
		{
			polynomial* temp = make_term(a->coeff, a->exp);
			append_poly(sum,temp); // add a to answer

			a_head = a_head->next;
			a = a_head;
			b = b_head;
		}
		else if( cmp( a->exp,b->exp)  == -1)
		{
			// loop to find the same exponent as a in b's list
			while((b) && (cmp( a->exp,b->exp) != 0))
			{
				b = b->next;
			}
			if(b != NULL) // A's exponent matched a later B's exponent
			{
				// b is now pointing at the like exponent.
				while(b_head != b) // add all elements from b_head until b.
				{
					polynomial* middleTerm = make_term(b_head->coeff, b_head->exp);
					append_poly(sum,middleTerm);
					remove_poly(&b_head);
				}

				sumCo  = ((a->coeff) + (b->coeff));
				polynomial* temp = make_term(sumCo, a->exp);

				append_poly(sum,temp);

				a_head = a_head->next;
				a = a_head;
				b = b_head->next;

			}
			else // A's exponent didn't match a later B. Adding b_head
			{
				polynomial* temp = make_term(b_head->coeff, b_head->exp);
				append_poly(sum,temp);

				a = a_head;
				b_head = b_head->next;
				b = b_head;
			}
		}

		if((!a) && b) // check for a greater number of terms in b than a
		{
			while(b)
			{
				polynomial* temp2 = make_term(b->coeff, b->exp);
				append_poly(sum,temp2);
				b = b->next;
			}
		}
	}

	remove_poly(&sum);
	return(sum);
}

polynomial* sub_poly(polynomial* a, polynomial* b)
{
	polynomial* diff = make_term(0,0);
	polynomial* a_head = a;
	polynomial* b_head = b;
	int sumCo = 0;

	while(b)
	{
		b->coeff *= -1;

		b = b->next;
	}
	b = b_head;

	while(a)
	{
		if( cmp( a->exp,b->exp)  == 0)
		{
			sumCo  = ((a->coeff) + (b->coeff));
			polynomial* temp = make_term(sumCo, a->exp);
			append_poly(diff,temp);

			a_head = a_head->next;
			a = a_head;

			if(b->next) // check for more b
			{
				b_head = b->next;
				b = b_head;
			}
			else
			{
				while(a) // no more b. adding rest of a to the answer
				{
					polynomial* temp2 = make_term(a->coeff, a->exp);
					append_poly(diff,temp2);
					a = a->next;
				}
			}
		}
		else if( cmp( a->exp,b->exp)  == 1)
		{
			polynomial* temp = make_term(a->coeff, a->exp);
			append_poly(diff,temp); // add a to answer

			a_head = a_head->next;
			a = a_head;
			b = b_head;
		}
		else if( cmp( a->exp,b->exp)  == -1)
		{
			// loop to find the same exponent as a in b's list
			while((b) && (cmp( a->exp,b->exp) != 0))
			{
				b = b->next;
			}
			if(b != NULL) // A's exponent matched a later B's exponent
			{
				// b is now pointing at the like exponent.
				while(b_head != b) // add all elements from b_head until b.
				{
					polynomial* middleTerm = make_term(b_head->coeff, b_head->exp);
					append_poly(diff,middleTerm);
					remove_poly(&b_head);
				}

				sumCo  = ((a->coeff) + (b->coeff));
				polynomial* temp = make_term(sumCo, a->exp);

				append_poly(diff,temp);

				a_head = a_head->next;
				a = a_head;
				b = b_head->next;
			}
			else // A's exponent didn't match a later B. Adding b_head
			{
				polynomial* temp = make_term(b_head->coeff, b_head->exp);
				append_poly(diff,temp);

				a = a_head;
				b_head = b_head->next;
				b = b_head;
			}
		}

		if((!a) && (b)) // check for a greater number of terms in b than a
		{
			while(b)
			{
				polynomial* temp2 = make_term(b->coeff, b->exp);
				append_poly(diff,temp2);
				b = b->next;
			}
		}
	}

	remove_poly(&diff);
	return(diff);
}

wchar_t* toSuper(int exponent)
{
	/* References: 
	 * https://linuxprograms.wordpress.com/tag/c-utf-8-handling/
	 * unicodebook.readthedocs.org/programming_languages.html#c-language
	 * https://en.wikipedia.org/wiki/Unicode_subscripts_and_superscripts
	 */

	char* buf = malloc(25);
	wchar_t* retVal = malloc(25);
	
	sprintf(buf,"%d",exponent);

	for(size_t i = 0; i < strlen(buf); i++) // in case the integer is >9
	{
		switch(buf[i])
		{
			case '0':
				memmove(&retVal[i],L"\u2070",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2070"); // 0
				break;
			case '1':
				memmove(&retVal[i],L"\u00B9",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u00B9"); // 1
				break;
			case '2':
				memmove(&retVal[i],L"\u00B2",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u00B2"); // 2
				break;
			case '3':
				memmove(&retVal[i],L"\u00B3",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u00B3"); // 3
				break;
			case '4':
				memmove(&retVal[i],L"\u2074",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2074"); // 4
				break;
			case '5':
				memmove(&retVal[i],L"\u2075",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2075"); // 5
				break;
			case '6':
				memmove(&retVal[i],L"\u2076",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2076"); // 6
				break;
			case '7':
				memmove(&retVal[i],L"\u2077",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2077"); // 7
				break;
			case '8':
				memmove(&retVal[i],L"\u2078",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2078"); // 8
				break;
			case '9':
				memmove(&retVal[i],L"\u2079",sizeof(wchar_t));
				// printf("Unicode char: [%ls]\n",L"\u2079"); // 9
				break;
		}
	}
	free(buf);
	return(retVal);
}

void transform(polynomial* p)
{
	p->coeff += 1;
	p->exp += 1;
}

void destroy_poly(polynomial* list)
{
	while(list)
	{
		polynomial* tmp = list;
		free(tmp);
		list = list->next;
	}
}

void remove_poly(polynomial** list) // remove first node
{
	polynomial *old_head = *list;
	*list = old_head->next;
	old_head->next = NULL;
	destroy_poly(old_head);
}

void append_poly(polynomial* a, polynomial* b)
{
	while(a->next)
	{
		a = a->next;
	}
	a->next = b;
}

void add_to_poly(polynomial** list, int coeff, int exp) // add node as head
{
	polynomial *item = make_term(coeff,exp);
	if(item)
	{
		item->next = *list;
		*list = item;
	}
}

int cmp(int x, int y)
{
	if(x < y)
	{
		return(-1);
	}
	else if(x > y)
	{
		return(1);
	}
	else
	{
		return(0);
	}
}

void removeNext(polynomial** list)
{
	polynomial *old_term = (*list)->next;
	(*list)->next = (*list)->next->next;
	old_term->next = NULL;
	destroy_poly(old_term);
}

struct term* make_term(int coeff, int exp)
{
	setlocale(LC_ALL, ""); // IMPORTANT: MUST BE CALLED BEFORE UTF-8 EXPONENTS CAN BE PRINTED

	struct term* node = malloc(sizeof(*node));
	if(node)
	{
		node->coeff = coeff;
		node->exp = exp;
		node->next = NULL;
	}
	return(node);
}

void poly_free(polynomial *eqn)
{
	while(eqn)
	{
		struct term *tmp = eqn->next;
		free(eqn);
		eqn = tmp;
	}
}

void poly_print(polynomial* eqn)
{
	if(!eqn)
	{
		return;
	}
	if(eqn->coeff)
	{
		printf("%c%d", eqn->coeff > 0 ? '+' : '\0', eqn->coeff);
		if(eqn->exp > 1)
		{
			wchar_t* expToPrint = toSuper((eqn->exp));
			printf("x");
			printf("%ls", expToPrint);
		}
		else if(eqn->exp == 1)
		{
			printf("x");
		}
		printf(" ");
	}
	poly_print(eqn->next);
}